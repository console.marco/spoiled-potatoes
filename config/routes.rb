Rails.application.routes.draw do
  devise_for :users
  #get 'movies/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #get "/movies", to:"movies#index"
  root "movies#index"
  resources :movies do
 	 resources :reviews
 	end

 resources :moviegoers
end

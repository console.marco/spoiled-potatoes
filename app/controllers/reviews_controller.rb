class ReviewsController < ApplicationController

  def edit
    @review = Movie.find(params[:movie_id]).reviews.find(params[:id])
  end

  def update
    @review = Movie.find(params[:movie_id]).reviews.find(params[:id])
    
    if @review.update(review_params)
      redirect_to @review.movie
    else
      render :edit
    end
  end

  def create
    @movie = Movie.find(params[:movie_id])
    

    @review = @movie.reviews.create(review_params)
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:movie_id])
    @review = @movie.reviews.find(params[:id])
    @review.destroy
    redirect_to movie_path(@movie)
  end

  private
    def review_params
      p = params.require(:review).permit(:body, :score, :moviegoer)
      {:body=> p[:body], :score=>p[:score], :moviegoer=>Moviegoer.find(p[:moviegoer])}
    end




end

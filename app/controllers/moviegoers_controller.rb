class MoviegoersController < ApplicationController

  def index
  	 @mvgs = Moviegoer.all
  end

  def show
    @mvg = Moviegoer.find(params[:id])
  end

  def new
  	@mvg = Moviegoer.new
  end

  def create
  	@mvg = Moviegoer.new(mvg_params)
  	if @mvg.save
      redirect_to @mvg
    else
      render :new
    end
  end

 def edit
    @mvg = Moviegoer.find(params[:id])
  end

def destroy
    @mvg = Moviegoer.find(params[:id])
    @mvg.destroy

    redirect_to root_path
  end

 def update
    @mvg = Moviegoer.find(params[:id])
    if @mvg.update(mvg_params)
      redirect_to @mvg
    else
      render :edit
    end
  end

	private
    def mvg_params
      params.require(:moviegoer).permit(:name, :bio)
    end


end

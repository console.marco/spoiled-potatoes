class Movie < ApplicationRecord
	has_many :reviews, dependent: :destroy 

	def averageScore
		
		total = 0
		self.reviews.each do |r|
			total += r.score
		end
		
		return (total.to_f/self.reviews.count.to_f).round(2)
	end 

end

class AddBioToMoviegoers < ActiveRecord::Migration[6.1]
  def change
    add_column :moviegoers, :bio, :text
  end
end

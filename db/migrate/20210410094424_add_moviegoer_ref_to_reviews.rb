class AddMoviegoerRefToReviews < ActiveRecord::Migration[6.1]
  def change
    add_reference :reviews, :moviegoer, null: false, foreign_key: true
  end
end

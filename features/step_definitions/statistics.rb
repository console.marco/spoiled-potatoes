Given(/^I am on the home page$/) do
  visit "/"
end

Given(/^Movie "(.*?)" with Director "(.*?)" and Year "(.*?)" is there$/) do |arg1,arg2,arg3|
  m = Movie.new( title: arg1, director: arg2, year: arg3)
  m.save
end

Given(/^Moviegoer "(.*?)" is there$/) do |arg1|
	rev = Moviegoer.new(name: arg1)
	rev.save
end

Given(/^Review "(.*?)" by Moviegoer "(.*?)" for Movie "(.*?)" with Score "(.*?)" is there$/) do |arg1,arg2,arg3,arg4|
  rev = Moviegoer.find_by( name: arg2)
  mov = Movie.find_by( title: arg3)
  r = Review.new(body: arg1, movie: mov, score: arg4, moviegoer: rev )
  r.save
end

Then(/^I should see table heading "(.*?)"$/) do |arg1|
   expect(page).to have_selector("th", text:arg1)
end

Then(/^I should see Movie "(.*?)" with Director "(.*?)" and Year "(.*?)" and Average Score "(.*?)" and Number of Reviews "(.*?)"$/) do |arg1,arg2,arg3,arg4,arg5|
	xpath_query = '//table/tr[td="'+arg1 +'" and td="'+arg2 +'" and td="'+arg3 +'" and td="'+arg5 +'"]'
	expect(page).to have_xpath(xpath_query)
end

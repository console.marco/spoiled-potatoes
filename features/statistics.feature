Feature: Statistics
  In order to see the movies
  As a viewer
  I want to see the Number of Reviews
  I want to see the Average Review Rating


	Scenario: View Number of Reviews per Movie
  		Given I am on the home page
  		Then I should see table heading "Number of Reviews"
  		Then I should see table heading "Average Score"

	Scenario: View Specific Number of Reviews and Average for TEST_MOVIE
		Given Movie "TEST_MOVIE" with Director "TEST_DIRECTOR" and Year "1900" is there
		Given Moviegoer "TEST_MOVIEGOER" is there
		Given Review "Rev1" by Moviegoer "TEST_MOVIEGOER" for Movie "TEST_MOVIE" with Score "10" is there
		Given Review "Rev2" by Moviegoer "TEST_MOVIEGOER" for Movie "TEST_MOVIE" with Score "10" is there
  		Given I am on the home page
  		Then I should see Movie "TEST_MOVIE" with Director "TEST_DIRECTOR" and Year "1900" and Average Score "10" and Number of Reviews "2"
  		


  		